<?php

/**
 * @file
 * API functions for the Bulk SMS module.
 */

/**
 * Sends a single SMS message to one or more recipients or groups.
 *
 * @param string|array $numbers
 *   One (if string) or more (if array) mobile numbers to send SMS message to.
 *   Numbers should be defined with country code, but without preceding
 *   '+' or '00' (for example 44123123456).
 * @param string $message
 *   Text of the message to be sent.
 * @param array $extra_post_fields
 *   Additional optional parameters to be sent to gateway, as defined in
 *   send_sms docs: https://www.bulksms.co.uk/docs/eapi/submission/send_sms/ .
 *
 * @return array
 *   Result of SMS sending, including following array keys:
 *   - status_code - result code of SMS sending,
 *   - status_message - meaning of result_code,
 *   - status_description - could contain additional useful information
 *     about the result,
 *   - batch_id - optional, depending on the error.
 */
function bulksms_send_sms($numbers, $message, array $extra_post_fields = []) {
  return _bulksms_call_api('send_sms', $numbers, $message, $extra_post_fields);
}

/**
 * Calculate the cost to send an SMS to one or more recipients or groups.
 *
 * @param string|array $numbers
 *   One (if string) or more (if array) mobile numbers to send SMS message to.
 *   Numbers should be defined with country code, but without preceding
 *   '+' or '00' (for example 44123123456).
 * @param string $message
 *   Text of the message to be sent.
 * @param array $extra_post_fields
 *   Additional optional parameters to be sent to gateway, as defined in
 *   send_sms docs: https://www.bulksms.co.uk/docs/eapi/submission/quote_sms/ .
 *
 * @return array
 *   Result of SMS sending, including following array keys:
 *   - status_code - result code of SMS sending,
 *   - status_message - meaning of result_code,
 *   - status_description - could contain additional useful information
 *     about the result,
 *   - quote_total - how many credits it willl cost to send given message.
 */
function bulksms_quote_sms($numbers, $message, array $extra_post_fields = []) {
  return _bulksms_call_api('quote_sms', $numbers, $message, $extra_post_fields);
}
