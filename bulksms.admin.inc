<?php

/**
 * @file
 * Admin callbacks for the Bulk SMS module.
 */

/**
 * Admin form callback.
 *
 * @param array $form
 *   A semi-populated array on which to build the form.
 * @param array $form_state
 *   The state information of the form.
 *
 * @return array
 *   The populated form array.
 */
function bulksms_admin_settings_form(array $form, array &$form_state) {
  // General gateway settings.
  $form['account'] = [
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  ];
  $form['account']['bulksms_regional_gateway'] = [
    '#type' => 'select',
    '#title' => 'Bulk SMS regional gateway',
    '#options' => _bulksms_regional_gateways(),
    '#default_value' => variable_get('bulksms_regional_gateway', ''),
    '#required' => TRUE,
  ];
  $form['account']['bulksms_port'] = [
    '#type' => 'select',
    '#title' => 'Port',
    '#options' => drupal_map_assoc([80, 5567]),
    '#default_value' => variable_get('bulksms_port', ''),
    '#required' => TRUE,
    '#description' => t('BulkSMS recommends that you use port 5567 instead of port 80, but your firewall could block access to this port. See <a href="@faq_url">FAQ</a> for more information.', [
      '@faq_url' => 'https://www.bulksms.co.uk/docs/eapi/submission/faq/',
    ]),
  ];
  $form['account']['bulksms_username'] = [
    '#type' => 'textfield',
    '#title' => t('Bulk SMS username'),
    '#default_value' => variable_get('bulksms_username', ''),
    '#required' => TRUE,
  ];
  $form['account']['bulksms_password'] = [
    '#type' => 'textfield',
    '#title' => t('Bulk SMS password'),
    '#default_value' => variable_get('bulksms_password', ''),
    '#required' => TRUE,
  ];
  $form['account']['bulksms_sender_id'] = [
    '#type' => 'textfield',
    '#title' => t('Bulk SMS Sender ID'),
    '#default_value' => variable_get('bulksms_sender_id', ''),
    '#description' => t('This facility becomes available after credits have been purchased for the first time on Bulk SMS website. To request a sender id, go to <a href="@change_sender_id_url">Change Sender ID</a> section of your User Account. Alphanumeric sender id is a route-specific feature.', [
      '@change_sender_id_url' => 'https://www.bulksms.co.uk/home/change_sender_id/',
    ]),
  ];
  // Debug settings.
  $form['debug'] = [
    '#type' => 'fieldset',
    '#title' => t('Debug settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];
  $form['debug']['bulksms_mode'] = [
    '#type' => 'select',
    '#title' => 'Operation mode',
    '#options' => [
      'live' => 'live mode',
      'test_succeed' => 'test mode - always succeed',
      'test_fail' => 'test mode - always fail',
    ],
    '#default_value' => variable_get('bulksms_mode', ''),
    '#description' => '"Live mode" will really send SMS messages. Both test modes are for testing purposes only: they do basic validation (username, password, etc) of your submission, but stop short of actually submitting the message, and return a successful status code as if the message was submitted successfully ("always succeed" mode) or a failure code ("always fail" mode).',
  ];
  $form['debug']['bulksms_debug_watchdog'] = [
    '#type' => 'checkbox',
    '#title' => 'Log debug information to watchdog',
    '#default_value' => variable_get('bulksms_debug_watchdog', FALSE),
    '#description' => 'If enabled, will print debug messages of values sent to / received from the gateway.',
  ];
  return system_settings_form($form);
}
